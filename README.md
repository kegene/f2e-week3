# f2e-week3 公車路線及站點資訊 （TDX 公車 api）
`vue3` `axios` `scss` `unplugin-icons` `leaflet`

## 簡介
本次網站主題是 關於台灣的公車行駛資訊，可查詢公車路線、動態資訊。

###  DEMO

https://kegene.gitlab.io/f2e-week3

## 項目資訊

主要:
- Node 12.16
- Vue 3
- Axios
- Leaflet
- Unplugin-icons
- Eslint
- Sass
- Typescript
- jsSHA

項目運行:
```
$ yarn
$ yarn serve
```

## 功能開發

- [x] 臺灣地區下拉功能
- [ ] 路線動態資訊
  - [X] 公車資訊
  - [X] 客運資訊
  - [X] 文字模式
  - [ ] 地圖模式
  - [X] 定時更新
- [ ] 站牌資訊
## 感謝名單

- 資料來源 來自 TDX運輸資料流通服務平臺 提供之api


