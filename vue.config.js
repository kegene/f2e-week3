const env = process.env;
const dev = { publicPath: "/" };
const test = { publicPath: "/dist" };
const prod = { publicPath: "/f2e-week3" };

const config =
  env.NODE_ENV === "production" ? prod : env.NODE_ENV === "test" ? test : dev;

module.exports = {
  configureWebpack: {
    plugins: [
      require("unplugin-icons/webpack")({
        /* options */
        compiler: "vue3",
      }),
    ],
  },
  ...config,
};
