import { ref, reactive, computed } from "vue";

export const direction = {
  0: {
    id: "outbound",
    label: "去程 - 起始",
  },
  1: {
    id: "returnTrip",
    label: "返程 - 終點",
  },
};
export const directionIndex = ref("outbound");

export const setDirectionIndex = (index) => {
  directionIndex.value = index;
};
export const useBusDynamicData = () => {
  const busDynamicData = reactive({});
  const busInfo = reactive({});
  const busCarList = ref([]);

  const busView = computed(() => {
    const {
      RouteName = {},
      DepartureStopNameZh = "",
      DestinationStopNameZh = "",
      RouteMapImageUrl,
      FareBufferZoneDescriptionZh,
    } = busInfo;
    const { Zh_tw } = RouteName;

    const tags = [FareBufferZoneDescriptionZh].filter((tag) => !!tag);

    return {
      title: `[${Zh_tw}] ${DepartureStopNameZh} - ${DestinationStopNameZh}`,
      image: RouteMapImageUrl,
      tags,
    };
  });

  const sortStopList = (dynamicList, stopList) => {
    const tmpAry = [];
    const busStopData = {
      outbound: [],
      returnTrip: [],
    };

    for (const stopItem of stopList) {
      const { Direction: stopDirection, Stops, RouteID, SubRouteID } = stopItem;
      if (SubRouteID.indexOf(RouteID) > -1) {
        busStopData[direction[stopDirection].id] = Stops;
      }
    }

    for (const dynamicItem of dynamicList) {
      const { Direction, StopID } = dynamicItem;
      const key = Direction === 0 ? "outbound" : "returnTrip";

      for (const stopItem of busStopData[key]) {
        if (StopID === stopItem.StopID && stopItem.PlateNumb !== "-1") {
          tmpAry.push({ ...dynamicItem, ...stopItem });
        }
      }
    }

    return tmpAry.sort((a, b) => a.StopSequence - b.StopSequence);
  };

  const setBusDynamicData = (dynamicList, stopList) => {
    const sortList = sortStopList(dynamicList, stopList);
    console.log(sortList);

    sortList.forEach((stopItem) => {
      const { Direction, StopSequence } = stopItem;
      if (!busDynamicData[direction[Direction].id]) {
        busDynamicData[direction[Direction].id] = [];
      }
      const BusCars = busCarList.value.filter(
        (item) =>
          item.StopSequence === StopSequence && item.Direction === Direction
      );
      busDynamicData[direction[Direction].id].push({
        ...stopItem,
        BusCars,
      });
    });
  };

  const initBusInfo = (data) => {
    Object.assign(busInfo, data);
  };

  const initBusDynamicData = () => {
    busDynamicData.outbound = [];
    busDynamicData.returnTrip = [];
  };
  const initBusCarList = (list) => {
    busCarList.value = list;
  };
  return {
    busInfo,
    initBusInfo,
    busDynamicData,
    setBusDynamicData,
    initBusDynamicData,
    busCarList,
    initBusCarList,

    busView,
  };
};
