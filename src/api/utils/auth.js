import jsSHA from "jssha";

export function getAuthorizationHeader() {
  const AppID = "afa8d3a83375411ea873c41e0b9bd87a";
  const AppKey = "DDvG9lKyn9sA34ef_OE8vxBSe5w";

  const GMTString = new Date().toUTCString();
  const ShaObj = new jsSHA("SHA-1", "TEXT");
  ShaObj.setHMACKey(AppKey, "TEXT");
  ShaObj.update("x-date: " + GMTString);
  const HMAC = ShaObj.getHMAC("B64");
  const Authorization =
    'hmac username="' +
    AppID +
    '", algorithm="hmac-sha1", headers="x-date", signature="' +
    HMAC +
    '"';

  return {
    // "Access-Control-Allow-Origin": "*",
    Authorization: Authorization,
    "X-Date": GMTString /*,'Accept-Encoding': 'gzip'*/,
  }; //如果要將js運行在伺服器，可額外加入 'Accept-Encoding': 'gzip'，要求壓縮以減少網路傳輸資料量
}
