export default {
  getKeyword: (keyword, props = ["Name", "Description"]) => {
    if (!keyword) {
      return;
    }
    return props
      .map((key) => `contains(${key},'${keyword}')`)
      .join("\u0020or\u0020");
  },
};
