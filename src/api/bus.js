import { $axios } from "./config/api-options";

export default {
  /**
   * GET /v2/Bus/Route/City/{City} 取得指定[縣市]的市區公車路線站序資料
   */
  getBusList(query) {
    const { City = "", ...other } = query;
    return $axios.motc.get(
      "/Bus/Route/City/{City}".replace(/\/{City}/, `/${City}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * /v2/Bus/StopOfRoute/City/{City}/{RouteName}
   */
  getBusStopList(query) {
    const { City = "", RouteName = "", ...other } = query;
    return $axios.motc.get(
      "/Bus/StopOfRoute/City/{City}/{RouteName}"
        .replace(/\{City}/, `${City}`)
        .replace(/{RouteName}/, `${RouteName}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * GET /v2/Bus/Route/City/{City}/{RouteName} 取得指定[縣市],[路線名稱]的路線資料
   */
  getBusInfo(query) {
    const { City = "", RouteName = "", ...other } = query;
    return $axios.motc.get(
      "/Bus/Route/City/{City}/{RouteName}"
        .replace(/\{City}/, `${City}`)
        .replace(/{RouteName}/, `${RouteName}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * /v2/Bus/EstimatedTimeOfArrival/Streaming/C
   * GET /v2/Bus/EstimatedTimeOfArrival/City/{City}/{RouteName} 取得指定[縣市],[路線名稱]的公車預估到站資料(N1)[批次更新
   */
  getBusDynamicList(query) {
    const { City = "", RouteName = "", ...other } = query;
    return $axios.motc.get(
      "/Bus/EstimatedTimeOfArrival/City/{City}/{RouteName}"
        .replace(/{City}/, `${City}`)
        .replace(/{RouteName}/, `${RouteName}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * /v2/Bus/RealTimeNearStop/City/{City}/{RouteName} 取得指定[縣市],[路線名稱]的公車動態定時資料(A2)[批次更新
   */
  getBusCarDynamicList(query) {
    const { City = "", RouteName = "", ...other } = query;
    return $axios.motc.get(
      "/Bus/RealTimeNearStop/City/{City}/{RouteName}"
        .replace(/{City}/, `${City}`)
        .replace(/{RouteName}/, `${RouteName}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * /v2/Bus/StopOfRoute/InterCity/{RouteName} 取得指定[路線名稱]的公路客運路線與站牌資料
   */
  getHighwayBusStopList(query) {
    const { RouteName = "", ...other } = query;
    return $axios.motc.get(
      "/Bus/StopOfRoute/InterCity/{RouteName}".replace(
        /{RouteName}/,
        `${RouteName}`
      ),
      {
        params: { ...other },
      }
    );
  },
  /**
   * GET /v2/Bus/EstimatedTimeOfArrival/InterCity/{RouteName} 公路客運路線資料
   */
  getHighwayBusList(query) {
    const { RouteName = "", ...other } = query;
    return $axios.motc.get(
      "/Bus/Route/InterCity/{RouteName}".replace(/{RouteName}/, `${RouteName}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * GET /v2/Bus/Route/InterCity/{RouteName} 取得指定[路線名稱]的公路客運路線資料
   */
  getHighwayBusInfo(query) {
    const { RouteName = "", ...other } = query;
    return $axios.motc.get(
      "/Bus/Route/InterCity/{RouteName}".replace(/{RouteName}/, `${RouteName}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * /v2/Bus/EstimatedTimeOfArrival/InterCity/{RouteName} 取得指定[路線名稱]的公路客運預估到站資料(N1)[批次更新
   */
  getHighwayBusDynamicList(query) {
    const { RouteName = "", ...other } = query;
    return $axios.motc.get(
      "/Bus/EstimatedTimeOfArrival/InterCity/{RouteName}".replace(
        /{RouteName}/,
        `${RouteName}`
      ),
      {
        params: { ...other },
      }
    );
  },
  /**
   * GET /v2/Bus/RealTimeNearStop/InterCity/{RouteName} 取得指定[路線名稱]的公車動態定時資料(A2)[批次更新
   */
  getHighwayCarDynamicList(query) {
    const { RouteName = "", ...other } = query;
    return $axios.motc.get(
      "/Bus/RealTimeNearStop/InterCity/{RouteName}".replace(
        /{RouteName}/,
        `${RouteName}`
      ),
      {
        params: { ...other },
      }
    );
  },
};
