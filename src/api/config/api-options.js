import useAxios from "@/utils/axios";
import { getAuthorizationHeader } from "../utils/auth";

const defaultOption = {
  headers: getAuthorizationHeader(),
};

const useOption = (options) => {
  for (const key of Object.keys(options)) {
    const option = options[key];
    options[key] = {
      ...defaultOption,
      ...option,
    };
  }
  return options;
};

const options = useOption({
  base: {
    baseURL: "https://link.motc.gov.tw/v2",
  },
  motc: {
    baseURL: "https://ptx.transportdata.tw/MOTC/v2",
  },
});

const $axios = {};

for (const key of Object.keys(options)) {
  $axios[key] = useAxios(options[key]);
}

const createAxios = (name) => {
  if ($axios[name]) {
    return;
  }
  $axios[name] = useAxios(options[name]);
};
const setAxios = (name) => {
  $axios[name] = useAxios(options[name]);
};
const updateAxios = (name, option) => {
  if (!options[name]) {
    return;
  }
  options[name] = { ...options[name], ...option };
  setAxios(name);
};

export { $axios, createAxios, updateAxios };
