export function getValueType(value) {
  const type = Object.prototype.toString.call(value);
  return type.replace(/.+ (\w+)]/g, "$1").toLowerCase();
}
export function isNumber(value) {
  return getValueType(value) === "number";
}
export function isString(value) {
  return getValueType(value) === "string";
}
export function isFunction(value) {
  return getValueType(value) === "function";
}
export function isPromise(value) {
  return getValueType(value) === "promise";
}
export function isDate(value) {
  return getValueType(value) === "date";
}
export function isObject(value) {
  return getValueType(value) === "object";
}
