import axios from "axios";

const useAxios = (options) => {
  const $axios = axios.create({ ...options });

  // 請求資料之前
  $axios.interceptors.request.use(
    function (config) {
      if (!config.params.$filter) {
        config.params.$filter = undefined;
      }
      config.params.$format = "json";
      return config;
    },
    function (error) {
      return Promise.reject(error);
    }
  );

  // 返回資料之前
  $axios.interceptors.response.use(
    function (response) {
      return /application\/json/.test(response["headers"]["content-type"])
        ? response.data
        : response;
    },
    function (error) {
      return Promise.reject(error);
    }
  );
  return $axios;
};

export default useAxios;
