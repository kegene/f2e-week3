import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";

const routes = [
  // 首頁 search bar, 選擇子頁
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  // 站牌資訊
  // 首頁 停靠該站牌的所有公車
  // 點擊公車，進入 公車或客運的詳情頁面
  {
    path: "/stop-sign",
    name: "stop-sign",
    component: () =>
      import(
        /* webpackChunkName: "StopSign" */ "../views/StopSign/StopSign.vue"
      ),
  },
  // 公車資訊
  // 首頁 所有客運
  // 點擊客運，進入詳情頁面
  // 詳情頁面 查看公車動態，價錢，時刻表，業者附圖
  {
    path: "/bus",
    name: "bus",
    component: () =>
      import(/* webpackChunkName: "Bus" */ "../views/Bus/Bus.vue"),
    children: [
      {
        path: "info",
        name: "bus-info",
        component: () =>
          import(/* webpackChunkName: "BusInfo" */ "../views/Bus/BusInfo.vue"),
      },
    ],
  },
  // 客運資訊
  // 首頁 所有公車
  // 點擊公車，進入詳情頁面
  // 詳情頁面 查看公車動態，價錢，時刻表，業者附圖
  {
    path: "/highway-bus",
    name: "highway-bus",
    component: () =>
      import(
        /* webpackChunkName: "HighwayBus" */ "../views/HighwayBus/HighwayBus.vue"
      ),
    children: [
      {
        path: "info",
        name: "highway-bus-info",
        component: () =>
          import(
            /* webpackChunkName: "HighwayBusInfo" */ "../views/HighwayBus/HighwayBusInfo.vue"
          ),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
